import React from 'react';
import io from 'socket.io-client';
import SocketIOFileUpload from 'socketio-file-upload';
let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImZyb250LmEudXNlci5nYWxsZXJ5QGdtYWlsLmNvbSIsInNlc3Npb25JZCI6MTksImlhdCI6MTU2OTgzNTExMSwiZXhwIjoxNTY5ODc4MzExfQ.h9MwATImJixSmqxbO5j0mPPiC7KxFQwt05YT5uhHACM';
// whoa, new version

class Upload extends React.Component {
    constructor() {
        super();
        this.percentage = 0;
        this.socket;
        // Setting up socket.io coPOSTnnection
        this.socket.on('tags_success', (tags) => {
            console.log(tags);

            // Connection socketio-fileupload to listen socket
            this.siofu = new SocketIOFileUpload(this.socket);

            // Adding event listener on progress event to show progress
            this.siofu.addEventListener('progress', (event) => {
                // will show 100 right away with small files.
                const percents = (event.bytesLoaded / event.file.size * 100).toFixed(2);
                console.log(`Percents loaded: ${percents}`);
            });

            this.siofu.addEventListener('start', (file) => {
                console.log('Upload started');
            })

            this.siofu.submitFiles(this.files);
        });
        this.socket.on('upload_fail', () => {
            console.log('Upload fail');
        })
        this.socket.on('upload_success', () => {
            console.log('Upload sucess');
        });
    }

    onFilesAdded(evt) {
        // SubmitFiles expects an array of files that is why
        // we are passing all selected files here
        // but we shouldnt
        this.files = evt.target.files;
        this.socket = io.connect('localhost:4000');

        this.onFilesAdded = this.onFilesAdded.bind(this);

        this.socket.on('authorize_fail', (err) => {
            console.log(`Authorization failed with error: ${err}`);
        })
        this.socket.on('authorize_success', (message) => {
            console.log(`Authorization succedded with message: ${message}`);
            this.socket.emit('tags', { year: '2019', category: 'ProvectusWeek' });
        })

        this.socket.emit('authorize', token);
    }

    render() {
        return (
            <div className="App">
                <h2>Choose file to start uploading</h2>
                <form>
                    <input id="fileInput" name="file" type="file" onChange={this.onFilesAdded} />
                </form>
            </div>
        );
    }
}

export default Upload;
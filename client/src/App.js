import React from 'react';
import './App.css';
import Upload from './upload/Upload'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Upload />
      </header>
    </div>
  );
}

export default App;
var SocketIOFileUpload = require('socketio-file-upload');
var socketio = require('socket.io');
var express = require('express');

const port = 5000;
 
// Make your Express server:
// var app = express()
//     .use(SocketIOFileUpload.router)
//     .listen(port, ()=> {
//         console.log(`Server is listening on ${port}`);
//     });

var app = express()
var server = require('http').createServer(app)
var io = socketio.listen(server);

app.get('/route', (req, res) => {
    res.send('route works on same port with socket');
})
 
// Start up Socket.IO:
io.on("connection", function(socket){
    // Make an instance of SocketIOFileUpload and listen on this socket:
    var uploader = new SocketIOFileUpload();
    uploader.dir = "./uploads";
    uploader.listen(socket);
 
    // Do something when a file is saved:
    uploader.on("saved", function(event){
        console.log('*** File saved ***');
        console.log(event.file);
    });
 
    // Error handler:
    uploader.on("error", function(event){
        console.log("Error from uploader", event);
    });
});

server.listen(port, ()=>{
    console.log(`Server is listening on port ${port}`)
});